unsigned long timer = 0, prev = 0;

void setup() {}

class Micro{
private:
  float value = 0;
  int GPIO;
  
public:
  Micro(int pin){
    GPIO = pin;
    pinMode(GPIO, INPUT);  
  }
  
  void read(){
    value = analogRead(A0) * 5 / 1023.0 * 100000;
  }
  
  bool isSignal(){
    return (value > 450) && (value < 1023);
  }
  
} micro(A0);

class Relay{
private:
    bool state = false;
    int GPIO;
public:
    Relay(int pin){
      GPIO = pin;
      pinMode(GPIO, OUTPUT);
      digitalWrite(GPIO, state);
    }
    
    void invert(){
      state = !state;
      digitalWrite(GPIO, state);
    }
} rel1(12);


void loop() {  
    micro.read();
    if (micro.isSignal()) {
        //time validation
        timer = millis() - prev;
        if ((timer > 200) && (timer < 1000)) rel1.invert();
        prev = millis();
    }
       
}